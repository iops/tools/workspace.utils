#!/usr/bin/env bash
# PURPOSE: run commands on all repos available in current dir & subdirs

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables

# -- auto: common helper functions
printTitle()  { echo;echo; msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
gitReposList() { find . -type d -name .git -exec dirname {} \; -follow; }
# -- /auto: common helper functions


# -- args parsing
# reset params
params=""
default_workspace="${PWD}"
# no params
[[ -z "${1}" ]] && NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -f|--force)
      FORCE=true
    ;;
    -h|--help)
      HELP=true
    ;;
    -v|--verbose)
      VERBOSE=true
    ;;
    -w|--workspace)
      shift
      WORKSPACE=${1}
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done


# script usage
if [[ -z "${params}" ]] || [[ "true" = ${HELP} ]] || [[ "true" = ${NOARGS} ]];then
  printTitle "USAGE"
  echo "[${scriptBaseName}] ${scriptName} [OPTIONS] <GIT_URL> [GIT_URL]"
  printTitle "OPTIONS"
  echo "-f|--force        	force update of repos list (do not use cache)"
  echo "-h|--help         	print out this help"
  echo "-v|--verbose      	enable verbose output"
  echo "-w|--workspace PATH	path to the root workspace (default:${default_workspace})"
  exit 0
fi

# clear screen first
clear

# set workpace
[[ -n "${WORKSPACE}" ]] || WORKSPACE="${default_workspace}"

# loop for all parameters
for l_git_url in ${params}
do

  # runtime vars
  if grep -q "http.:" <<<${l_git_url};then
    l_dst_dir=${WORKSPACE}/$(dirname ${l_git_url#http*://*/} 2>/dev/null)
    l_git_dir=${l_dst_dir}/$(basename ${l_git_url#http*://*/} .git 2>/dev/null)
  elif grep -q "ssh:" <<<${l_git_url};then
    l_dst_dir=${WORKSPACE}/$(dirname ${l_git_url#ssh*://*/} 2>/dev/null)
    l_git_dir=${l_dst_dir}/$(basename ${l_git_url#ssh*://*/} .git 2>/dev/null)
  else
    l_dst_dir=${WORKSPACE}/$(dirname ${l_git_url##*:} 2>/dev/null)
    l_git_dir=${l_dst_dir}/$(basename ${l_git_url##*:} .git 2>/dev/null)
  fi

  # setup destination dir & clone repository
  if [[ ! -d "${l_git_dir}" ]];then
    echo "[>] cloning [${l_git_url}] into ${l_dst_dir}"
    [[ -d ${l_dst_dir} ]] || mkdir -p ${l_dst_dir}
    cd ${l_dst_dir} && git clone "${l_git_url}"
  else
    echo "[i] ${l_git_dir} already exists, updating only..."
    cd ${l_git_dir} && git fetch -p
  fi

  # show status
  cd ${l_git_dir} && echo "[i] BRANCHES:" && git branch -a && echo "[i] STATUS:" && git status
  echo -e "[i] GOTO-PROJECT:\n--\npushd ${l_git_dir}\n--\n"

done


# eof
