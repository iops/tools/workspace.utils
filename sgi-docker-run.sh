#!/usr/bin/env bash
# PURPOSE: wrapper-script for starting sgi docker containers

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables

# -- auto: common helper functions
printTitle()  { echo;echo; msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
# -- /auto: common helper functions


# -- args parsing
# reset params
params=""
[[ -n "${WORKSPACE}" ]] || WORKSPACE="${PWD}"
# no params
[[ -z "${1}" ]] && NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -h|--help)
      HELP=true
    ;;
    -w|--workspace)
      shift
      WORKSPACE=${1}
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done


# script usage
if [[ -z "${params}" ]] || [[ "true" = ${HELP} ]] || [[ "true" = ${NOARGS} ]];then
  printTitle "USAGE"
  echo "${scriptName} [OPTIONS] <image> [command] [args]"
  printTitle "OPTIONS"
  echo "-h|--help         	print out this help"
  echo "-w|--workspace PATH	path to map to /workspace inside container (default: ${WORKSPACE})"
  printTitle "EXAMPLES"
  echo "${scriptName} sgnexus02.scigames.at:5001/common/base-sgbuilds:develop"
  echo
  exit 0
fi

# clear screen first
clear


# start container
docker run \
  --name sgi-test-container \
  --hostname sgi-test-container \
  --rm -it \
  -u $(id -u) \
  -e http_proxy -e https_proxy \
  -e MAVEN_OPTS="-Duser.home=/" \
  -e PS1='[\h: \W]\$ ' \
  -e SETTINGS_PATH=/.m2/settings.xml \
  -v ${HOME}/.m2:/.m2 \
  -v ${PWD}:/workspace \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --entrypoint /bin/bash ${params}


# eof
