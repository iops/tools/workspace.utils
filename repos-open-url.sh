#!/usr/bin/env bash
# PURPOSE: open current repo's url in default browser

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables

# -- auto: common helper functions
printTitle()  { echo;echo; msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
# -- /auto: common helper functions


# -- args parsing
# reset params
params=""
# get repo's server url from origin remote
git_repo_url=$(git remote -v 2>/dev/null|grep "origin.*fetch"|sed -e "s#:#/#" -e "s#.*@#http://#" -e "s#\.git.*#/-/network/master#")
# default command to open url (osx)
url_open_command="open"
# wsl-speciific command
[[ -x /usr/bin/wslview ]] && url_open_command="/usr/bin/wslview"
# no params
#[[ -z "${1}" ]] && NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -h|--help)
      HELP=true
    ;;
    -c|--open-command)
      shift
      url_open_command=${1}
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done


# script usage
if [[ "true" = ${HELP} ]] || [[ "true" = ${NOARGS} ]];then
  printTitle "USAGE"
  echo "${scriptName} [OPTIONS]"
  printTitle "OPTIONS"
  echo "-h|--help         	        print out this help"
  echo "-c|--open-command COMMAND	  command to open url in browser (default:${url_open_command})"
  exit 0
fi


# open url if git repo url is found
if [[ -n "${git_repo_url}" ]];then
  echo "[>] opening ${git_repo_url} in default browser (using ${url_open_command})..."
  ${url_open_command} ${git_repo_url}
else
  echo "[x] could not detect git-url (not a git workspace?)"
fi


# eof
