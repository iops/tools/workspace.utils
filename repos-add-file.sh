#!/usr/bin/env bash
# PURPOSE: add a file from our repo file-templates to current working directory

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables

# -- auto: common helper functions
printTitle()  { echo;echo; msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
# -- /auto: common helper functions


# -- args parsing
# reset params
params=""
# where to copy file to
dst_directory="${PWD}"
# where to copy file from
src_directory="${scriptFullDir}/files/repo"
# no params
[[ -z "${1}" ]] && NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -f|--force)
      FORCE=true
    ;;
    -h|--help)
      HELP=true
    ;;
    -v|--verbose)
      VERBOSE=true
    ;;
    -d|--dst-dir)
      shift
      DST_DIR=${1}
    ;;
    -s|--src-dir)
      shift
      SRC_DIR=${1}
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done


# script usage
if [[ -z "${params}" ]] || [[ "true" = ${HELP} ]] || [[ "true" = ${NOARGS} ]];then
  printTitle "USAGE"
  echo "[${scriptBaseName}] ${scriptName} [OPTIONS] <SRC_FILENAME> [SRC_FILENAME]"
  printTitle "OPTIONS"
  echo "-f|--force        	override existing files"
  echo "-h|--help         	print out this help"
  echo "-v|--verbose      	enable verbose output"
  echo "-d|--dst-dir PATH	  where to put file to    (default:${dst_directory})"
  echo "-s|--src-dir PATH	  where to copy file from (default:${src_directory})"
  echo "LIST OF EXISTING SOURCE FILES:"
  echo "--"
  (cd ${src_directory} && find . -type f -exec basename {} \;)
  echo "--"
  exit 0
fi

# set source & destination directory
[[ -n "${DST_DIR}" ]] || DST_DIR="${dst_directory}"
[[ -n "${SRC_DIR}" ]] || SRC_DIR="${src_directory}"

# loop for all parameters
for l_src_file in ${params}
do
  # ensure to use filename only
  l_src_file=$(basename ${l_src_file})
  # compute destination file
  l_dst_file="${DST_DIR}/${l_src_file}"

  # ask for confirmation if destination file already exists
  if [[ -f "${l_dst_file}" ]];then
    echo "[i] destination file [${l_dst_file}] already exists"
    if [[ "true" != ${FORCE} ]];then
      echo -n "[?] overwrite existing file? [y/n] "
      read -n 1 -r
      echo
      if [[ ${REPLY} =~ ^[Yy]$ ]];then
        echo "[!] overwriting file [${l_src_file}] in [${DST_DIR}]"
      else
        echo "[!] skipping file [${l_src_file}] in [${DST_DIR}]"
        continue
      fi
    fi
  fi

  # if file exists, copy it
  if [[ -f "${SRC_DIR}/${l_src_file}" ]];then
    echo "[>] copying [${l_src_file}] from [${SRC_DIR}] to [${DST_DIR}]"
    cp -v "${SRC_DIR}/${l_src_file}" "${DST_DIR}"
  else
    echo "[x] file [${l_src_file}] does not exist in [${SRC_DIR}]"
  fi

done


# eof
